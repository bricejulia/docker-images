#!/bin/bash
set -e

uid=$(stat -c %u /srv)
gid=$(stat -c %g /srv)

if [ $uid == 0 ] && [ $gid == 0 ]; then
    if [ $# -eq 0 ]; then
        php-fpm
    else
        exec "$@"
    fi
fi

if [ ! "production" == "$APP_ENV" ] && [ ! "prod" == "$APP_ENV" ]; then
    # Enable xdebug
else
    # Disable xdebug

    ## FPM
    if [ -e /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini ]; then
        rm -f /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini
    fi
fi


sed -i -r "s/foo:x:\d+:\d+:/foo:x:$uid:$gid:/g" /etc/passwd
sed -i -r "s/bar:x:\d+:/bar:x:$gid:/g" /etc/group
chown $uid:$gid /home

if [ $# -eq 0 ]; then
    php-fpm
else
    su foo -c "$@"
fi
